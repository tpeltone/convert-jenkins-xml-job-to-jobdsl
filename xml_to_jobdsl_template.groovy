def jobconfig = """
XML JOB HERE
"""

def jobconfignode = new XmlParser().parseText(jobconfig)

job('replace-me-jobdsl') {
    configure { node ->
        // node represents <project>
        jobconfignode.each { child ->

          def name = child.name()

          def existingChild = node.get(name)
          if(existingChild){
            node.remove(existingChild)
          }

          node << child
        }
    }
}
